package main

import (
	"flag"
	"net"
	"net/http"
	"net/rpc"
	"net/rpc/jsonrpc"

	log "github.com/Sirupsen/logrus"

	"bitbucket.org/czertboot/protocol"
)

func main() {
	httpAddr := flag.String("httpaddr", ":8080", "Listen address HTTP server")
	rpcAddr := flag.String("rpcaddr", ":8081", "Listen address RPC server")

	flag.Parse()

	log.SetFormatter(&log.TextFormatter{ForceColors: true})
	log.SetLevel(log.DebugLevel)

	go rpcServer(*rpcAddr)
	httpServer(*httpAddr)
}

// RPC Server
func rpcServer(addr string) {
	log.WithField("addr", addr).Infof("Starting CzertBoot RPC Server")

	server := rpc.NewServer()
	server.Register(new(protocol.Foo))
	server.HandleHTTP(rpc.DefaultRPCPath, rpc.DefaultDebugPath)

	l, e := net.Listen("tcp", addr)
	if e != nil {
		log.Fatal("listen error:", e)
	}

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}

		go server.ServeCodec(jsonrpc.NewServerCodec(conn))
	}
}

// HTTP Server
func httpServer(addr string) {
	log.WithField("addr", addr).Infof("Starting CzertBoot HTTP Server")

	http.Handle("/foo", &fooHandler{})
	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("bar"))
	})

	log.Fatal(http.ListenAndServe(addr, nil))
}

type fooHandler struct{}

func (fh *fooHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("foo"))
}
