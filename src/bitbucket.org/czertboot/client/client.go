package main

import (
	"flag"
	"net"
	"net/rpc/jsonrpc"

	log "github.com/Sirupsen/logrus"

	"bitbucket.org/czertboot/protocol"
)

func main() {
	rpcAddr := flag.String("rpcaddr", ":8081", "Host address RPC server")

	flag.Parse()

	log.SetFormatter(&log.TextFormatter{ForceColors: true})
	log.SetLevel(log.DebugLevel)

	log.WithField("addr", *rpcAddr).Infof("Running CzertBoot RPC Client")

	conn, err := net.Dial("tcp", *rpcAddr)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer conn.Close()

	client := jsonrpc.NewClient(conn)
	if err != nil {
		log.Fatal("dialing:", err)
	}

	fooRsp := new(protocol.FooRsp)
	fooArgs := protocol.FooArgs{
		Value: "Hello",
	}

	if err := client.Call("Foo.Compute", fooArgs, fooRsp); err != nil {
		log.WithField("err", err).Fatal("calling Foo.Compute failed")
	}

	log.WithField("response", fooRsp.Value).Infof("RPC call")
}
