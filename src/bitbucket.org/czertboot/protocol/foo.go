package protocol

import (
	"fmt"

	log "github.com/Sirupsen/logrus"
)

// JSON RPC
type FooArgs struct {
	Value string `json:"value"`
}

type FooRsp struct {
	Value string `json:"value"`
}

type Foo int

func (f *Foo) Compute(args FooArgs, rsp *FooRsp) error {
	log.WithField("value", args.Value).Infof("Foo.Compute")
	rsp.Value = fmt.Sprintf("Rsp: %s", args.Value)

	return nil
}
