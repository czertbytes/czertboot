# Build script

linux-all:
	GOARCH=amd64 GOOS=linux CGO_ENABLED=0 gb build all
	make -C src/bitbucket.org/czertboot/client
	make -C src/bitbucket.org/czertboot/server

clean:
	make -C src/bitbucket.org/czertboot/client clean
	make -C src/bitbucket.org/czertboot/server clean

deps:
	go get github.com/constabulary/gb/...

# This is only helper, should be done manually!
fetch-logrus:
	mkdir -p vendor/src/
	git clone https://github.com/Sirupsen/logrus vendor/src/github.com/Sirupsen/logrus
